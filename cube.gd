tool

extends MeshInstance

export(float, 0, 10) var speed_roty setget _speed_roty

func _speed_roty( f ):
	speed_roty = f

func _ready():
	pass

func _process(delta):
	rotate_y( delta * speed_roty )